package edu.Purdue.study.Software_Design;

import java.math.BigDecimal;
/**
 * 
 * @author Mounika
 * This Class gives the calculated total of the items.
 */
public class CashRegisterCalculate {
	/**
	 * It is the cost of the per session
	 */
	private static double totalCost = 0;
	
	public CashRegisterCalculate()
	{
		
	}
	public CashRegisterCalculate(Item items)
	{
		calculate(items);
	}
	
	/**
	 * It calculates the total cost with newly added Item's cost.
	 * @param items The newly added item.
	 */
	public BigDecimal calculate(Item items) {
		/**
		 * It is the discount chosen by the user.
		 */
		int discount = 0;
		/**
		 *  It is the Item cost given by the user.
		 */
		double perItemCost = 0;
		/**
		 * It is the item cost according to quantity without discount.
		 */
		double WithoutDiscountCost = 0;
		/**
		 * It is the rounded off total price.
		 */
		BigDecimal actualTotal = null;
		WithoutDiscountCost = items.getQuantity() * items.getCost();
		discount = getDiscountOff(items.getDiscount());
		int dis = (100-discount) ;
		perItemCost = (dis * (WithoutDiscountCost));
		perItemCost = perItemCost/100 ; 
		totalCost = totalCost + perItemCost;
		//totalCost = Math.round(totalCost * 100)/100 ;
		actualTotal =  new BigDecimal(totalCost).setScale(2, BigDecimal.ROUND_HALF_UP);
		System.out.println("Total : " +actualTotal);	
		return actualTotal;
	}
	/**
	 * It has the discount options like 10% off, 20% off etc., 
	 * @param discountOption User selected option
	 * @return returns 10% , 20% off discount according to user selection.
	 */
	public int getDiscountOff(int discountOption)
	{
		int dis = 0;
		switch(discountOption)
		{
		case 1 : dis = 10;
		break;
		case 2 : dis = 20;
		break;
		default : dis = 0;
		break;
		}
		return dis;
	}
}
