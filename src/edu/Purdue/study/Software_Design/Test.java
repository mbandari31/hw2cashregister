package edu.Purdue.study.Software_Design;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		/**
		 * It is used to find whether user wants to add another Item.
		 */
		int itemsCount= 0;
		/**
		 * It is used to find the total count of the items added by the user.
		 */
		int count = 0;
		/**
		 * It is used to calculate the total cost of the Items.
		 */
		CashRegisterCalculate calc = null;
		/**
		 * It is used to add items dynamically by the user.
		 */
		ArrayList<Item> items = null;
		/**
		 * It is used to take the details of each item.
		 */
		Item eachItem = null;
		System.out.println("\n Enter 1 for item price to enter each time");
		Scanner scan = new Scanner(System.in).useLocale(Locale.US);
		itemsCount = scan.nextInt();
		//Item[] items = new Item[itemsCount];
		if(itemsCount == 1)
		{
			items = new ArrayList<Item>();
			eachItem = null;
			do
			{
				count++;
				eachItem = new Item();
				System.out.println("\n Enter the unit price for item no." +count);
				double cost = scan.nextDouble();
				eachItem.setCost(cost);
				System.out.println("\n Enter the quantity of item no." +count);
				eachItem.setQuantity(scan.nextInt());
				System.out.println("\n Enter the discount for item no. \n 1. 10% off \n 2. 20% off" +count);
				eachItem.setDiscount(scan.nextInt());
				items.add(eachItem);
				if(eachItem != null)
					calc = new CashRegisterCalculate(eachItem);
				System.out.println("\n next press 1 or 0 to terminate");
				itemsCount = scan.nextInt();
			}while(itemsCount == 1);	
		}
	}
}
