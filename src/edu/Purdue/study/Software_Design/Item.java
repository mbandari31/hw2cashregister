package edu.Purdue.study.Software_Design;
/**
 * 
 * @author Mounika
 * Item Class is consists of Item Cost, Quantity and Discount. 
 */
public class Item {
	/**
	 * Cost of the Item
	 */
	private double cost;
	/**
	 * Quantity of Item
	 */
	private int quantity;
	/**
	 * Discount per Item
	 */
	private int discount;
	
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getQuantity() {
		return quantity;
	}
	public int getDiscount() {
		return discount;
	}	
}
