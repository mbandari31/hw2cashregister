package edu.Purdue.study.Software_Design;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class CashRegisterCalculateTest {

	@Test
	public void calculate() {
		Item items = new Item();
		items.setCost(2);
		items.setQuantity(3);
		items.setDiscount(1);
		CashRegisterCalculate cal = new CashRegisterCalculate();
		BigDecimal costActual = cal.calculate(items);
		
		
		BigDecimal costExpected = new BigDecimal("5.40");
		assertEquals(costExpected, costActual);
		
		int actualDiscount = cal.getDiscountOff(2);
		int estimatedDiscount = 20;
		assertEquals(estimatedDiscount, actualDiscount);
		
	}

}
